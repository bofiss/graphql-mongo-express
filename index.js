import express from 'express'
import bodyParser from 'body-parser'
import graphql from 'graphql'
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express'
import { makeExecutableSchema } from 'graphql-tools'
import typeDefs from './schema'
import resolvers from './resolvers'
import mongoose from 'mongoose';
var app = express()

// Put together a schema
const schema = makeExecutableSchema({
    typeDefs,
    resolvers,
})
  


mongoose.connect('mongodb://localhost/test');
const Cat = mongoose.model('Cat', { name: String });



// The GraphQL endpoint
app.use('/graphql', bodyParser.json(), graphqlExpress({ schema, context: { Cat } }))

// GraphiQL, a visual editor for queries
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }))

// Start the server
app.listen(3000, () => {
  console.log('Go to http://localhost:3000/graphiql to run queries!')
})



