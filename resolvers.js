
// The resolvers
const resolvers = {
  Query: {
      allCats: async(parents, args, {Cat}) => {
          const cats= await Cat.find({})
          return cats.map((cat) => {
              cat._id = cat._id.toString()
              return cat
          })
      },

      cat: async(parents, args, {Cat}) => {
        const cat = await Cat.findById({_id: args.id})
        cat._id = cat._id.toString()
        return cat        
      }

  },

  Mutation: {

    createCat: async(parents, args, {Cat}) => {
        const cat= await new Cat(args).save()
        cat._id = cat._id.toString()
        return cat        
    },

    deleteCat: async(parents, args, {Cat}) => {
        const cat= await Cat.findOneAndRemove({_id: args.id})
        return cat    
    },

    updateCat: async(parents, args, {Cat}) => {
        const cat = await Cat.findById({_id: args.id})
        cat.name = args.name
        cat.save()
        cat._id = cat._id.toString()
        return cat    
    }

  }

}

export default resolvers
  