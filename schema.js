export default`
    type Cat {
        _id: String!,
        name: String!
    }

    type Query {
        allCats: [Cat!]!
        cat(id: String!): Cat!
    }

    type Mutation {
        createCat(name: String!): Cat!
        deleteCat(id: String!): Cat!
        updateCat(id: String!, name: String!): Cat!
    }
`
